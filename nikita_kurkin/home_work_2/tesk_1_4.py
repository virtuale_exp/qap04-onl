import statistics


digits = 8, 6
average_ar = statistics.mean(digits)
average_geom = statistics.geometric_mean(digits)
print(average_ar, ', ', average_geom, sep='')
